terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "~> 2.13.0"
    }
  }
}

provider "docker" {} # who needs docker when they can have jails

resource "docker_image" "nginx" { // again jails not lxc
  name         = "nginx:latest"
  keep_locally = true    // keep image after "destroy"
}
/* I like
PB&J
*/
resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "tutorial"
  ports {
    internal = 80
    external = 2224
  }
}
